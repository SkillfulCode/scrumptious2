# we get data from the database by using a model class
from django.db import models
# this is to import the settings for the user model
from django.conf import settings


# the models package has a model class in it that our model classes need to inherit from
class Recipe(models.Model):
    # we add attributes to our Recipe model class
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    # the auto_now_add argument automatically sets the field to now when the object is first craeted
    created_on = models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        # this is the recommended way to refer to the User model
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        # if null is set to true, Django will put an empty value in the database if there is no value
        null=True,
    )

    # this lets us see a recipe objects title in the admin dropdown when adding steps
    # def __str__(self) is a method for string representatino of objects
    def __str__(self):
        return self.title

# every time we write a new model class, we need to make migrations so django can keep track of our models
# once we make migrations, we describe the changes to the models and database, but django doesn't make those changes yet
# the migrate command changes the database to the latest version
# You can also do these things backwards.
# If you want to undo the migrations for your recipes Django app, you can type the zero command


class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name="steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["step_number"]


class Ingredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )

    # defaults the ordering to food item
    class Meta:
        ordering = ['food_item']
